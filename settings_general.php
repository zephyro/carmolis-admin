<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>Общие настройки</span>
                            </h1>
                        </div>
                        <div class="heading__row_right">

                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">

					<div class="profile">

						<div class="profile__nav">
							<ul>
								<li class="active"><a href="#">Призы</a></li>
								<li><a href="#">Отзывы</a></li>
							</ul>
						</div>

						<div class="profile__content">

							<div class="row">
								<div class="col col-xs-12 col-lg-3 col-xl-3"></div>
								<div class="col col-xs-12 col-lg-9 col-xl-9">
									<h3>ДОБАВЛЕНИЕ И ИЗМЕНЕНИЕ ИНФОРМАЦИИ О ПРИЗАХ</h3>
								</div>
							</div>

							<div class="prize">

								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3"></div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<div class="prize__heading">
											<div class="prize__heading_left">
												<h3>ПРИЗ</h3>
												<div class="prize__nav"><a class="prize__nav_up" href="#">вверх</a> | <a class="prize__nav_down" href="#">вниз</a></div>
											</div>
											<div class="prize__heading_right"><a class="prize__remove" href="#">удалить приз</a></div>
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Периодичность</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<div class="row">
												<div class="col col-xs-12 col-sm-6 col-md-5 col-lg-5 col-xl-5">
													<input type="text" class="form_control" name="" value="" placeholder="">
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Название</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="2 билета в театр" placeholder="">
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Изображение в кругу</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<div class="form_group">
												<label class="form_file">
													<input class="form_file__input" type="file" name="" placeholder="" value="">
													<span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
													<span class="form_file__text"><span></span></span>
													<span class="form_file__btn">Загрузить</span>
												</label>
											</div>
											<div class="profile__photo">
												<div class="profile__photo_item">
													<label class="photo_form">
														<input type="file" class="">
														<span>
														<img src="images/user_photo.jpg" class="img-fluid" alt="">
													</span>
													</label>
												</div>
												<div class="profile__photo_text">
													<a class="profile__photo_remove" href="#">удалить фото</a>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Для кого</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="СТАТУС “УЧЕНИК”" placeholder="">
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">КОЛИЧЕСТВО</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="5 САМЫХ АКТИВНЫХ" placeholder="">
										</div>
									</div>
								</div>

							</div>

							<div class="profile_divider"></div>

							<div class="prize">

								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3"></div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<div class="prize__heading">
											<div class="prize__heading_left">
												<h3>ПРИЗ</h3>
												<div class="prize__nav"><a class="prize__nav_up" href="#">вверх</a> | <a class="prize__nav_down" href="#">вниз</a></div>
											</div>
											<div class="prize__heading_right"><a class="prize__remove" href="#">удалить приз</a></div>
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Периодичность</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<div class="row">
												<div class="col col-xs-12 col-sm-6 col-md-5 col-lg-5 col-xl-5">
													<input type="text" class="form_control" name="" value="" placeholder="">
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Название</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="2 билета в театр" placeholder="">
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Изображение в кругу</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<div class="form_group">
												<label class="form_file">
													<input class="form_file__input" type="file" name="" placeholder="" value="">
													<span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
													<span class="form_file__text"><span></span></span>
													<span class="form_file__btn">Загрузить</span>
												</label>
											</div>
											<div class="profile__photo">
												<div class="profile__photo_item">
													<label class="photo_form">
														<input type="file" class="">
														<span>
														<img src="images/user_photo.jpg" class="img-fluid" alt="">
													</span>
													</label>
												</div>
												<div class="profile__photo_text">
													<a class="profile__photo_remove" href="#">удалить фото</a>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Для кого</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="СТАТУС “УЧЕНИК”" placeholder="">
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">КОЛИЧЕСТВО</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="5 САМЫХ АКТИВНЫХ" placeholder="">
										</div>
									</div>
								</div>

							</div>

							<div class="profile_divider"></div>

							<div class="row mb-30">
								<div class="col col-xs-12 col-lg-3 col-xl-3"></div>
								<div class="col col-xs-12 col-lg-9 col-xl-9">
									<button type="submit" class="btn btn_blue btn_send">ДОБАВИТЬ</button>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">ОПИСАНИЕ “О ПРИЗАХ”</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<textarea class="form_control" name="name" placeholder="" rows="4"></textarea>
									</div>
								</div>
							</div>

							<div class="text-right">
								<button type="submit" class="btn btn_blue btn_send">СОХРАНИТЬ</button>
							</div>

						</div>

					</div>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
