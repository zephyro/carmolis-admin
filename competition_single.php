<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>Конкурс</span>
                            </h1>
                        </div>
                        <div class="heading__row_right">
	                        <a href="#" class="btn">ИЗМЕНИТЬ НАСТРОЙКИ КОНКУРСА</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">

	                <div class="competition">
		                <h3>РАБОТА “ДА БУДЕТ СВЕТ”</h3>

		                <div class="row">
			                <div class="col col-xs-12 col-md-6">
				                <a href="images/work.png" class="competition__image" data-fancybox="gallery">
					                <img src="images/work.png" class="img-fluid">
				                </a>
			                </div>
			                <div class="col col-xs-12 col-md-6">
				                <div class="competition__info">
					                <div class="h3">Дата размещение</div>
					                <p>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></p>
					                <div class="h3">Кто подтвердил</div>
					                <p>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></p>
					                <div class="h3">Управлять</div>
					                <div class="form_group">
						                <div class="form_label">Комментарий для пользователя</div>
						                <textarea class="form_control" name="" placeholder="" rows="3"></textarea>
					                </div>
					                <button type="submit" class="btn btn_border_rose btn_sm">УДАЛИТЬ РАБОТУ</button>
				                </div>
			                </div>
		                </div>

	                </div>

	                <h3>Проголосовали за работу</h3>
	                <ul class="competition__voice">
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
		                <li>04.03.2018, 15:32:40 | <a href="#">Алекс Пушков</a></li>
	                </ul>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>

