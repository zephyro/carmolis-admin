<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>МОДУЛЬ</span>
                                <sub class="color-red">архив модулей</sub>
                            </h1>
                        </div>
                        <div class="heading__row_right">
                            <a href="#" class="btn">ДОБАВИТЬ МОДУЛЬ</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">
                    <h2>Ученик</h2>
                    <div class="table_responsive">
                        <table class="table">
                            <tr>
                                <th>Поз.</th>
                                <th>Название</th>
                                <th>Короткое описание</th>
                                <th>Тип</th>
                                <th class="text-right">Подробней</th>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li></li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>1-я презентация</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Викторина - выиграй миллион</td>
                                <td>Короткое описание</td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li></li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Marilyn Castro</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Jacqueline Thomas</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>1-я презентация</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>1-я презентация</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Викторина - выиграй миллион</td>
                                <td>Короткое описание</td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Marilyn Castro</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Jacqueline Thomas</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>1-я презентация</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                    <h2>Специалист</h2>
                    <div class="table_responsive">
                        <table class="table">
                            <tr>
                                <th>Поз.</th>
                                <th>Название</th>
                                <th>Короткое описание</th>
                                <th>Тип</th>
                                <th class="text-right">Подробней</th>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li></li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>1-я презентация</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Викторина - выиграй миллион</td>
                                <td>Короткое описание</td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li></li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Marilyn Castro</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Jacqueline Thomas</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>1-я презентация</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>1-я презентация</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Викторина - выиграй миллион</td>
                                <td>Короткое описание</td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Marilyn Castro</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Jacqueline Thomas</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>1-я презентация</td>
                                <td>Короткое описание </td>
                                <td>Презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
