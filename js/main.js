// SVG IE11 support
svg4everybody();

// --- Top Nav
$(function() {
    var pull = $('.header__toggle');
    var menu = $('.page');

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('nav_open');
    });
});


$(".btn_modal").fancybox({
    'padding'    : 0
});



$('.sort').on('click touchstart', function(e){
    e.preventDefault();
    $(this).toggleClass('active');
});


$('.form_file input[type="file"]').on('change', function(e) {
    var box = $(this).closest('.form_file');
    var str = $(this).val();

    if (str.lastIndexOf('\\')){
        var i = str.lastIndexOf('\\')+1;
    }
    else{
        var i = str.lastIndexOf('/')+1;
    }
    var filename = str.slice(i);
    box.find('.form_file__text').text(filename);
});



$('.test_change label').on('click', function(e) {
   var  test = '.type' + $(this).find('input').val();
   $('.page').find('.test_type').removeClass('active');
   $('.page').find(test).addClass('active');
   console.log(test);
});


jQuery(document).ready(function () {
    $('.data_table').DataTable({
        "language": {
            "lengthMenu": "Показывать по _MENU_ строк",
            "zeroRecords": "Ничего не найдено",
            "info": "Показано _PAGE_ из _PAGES_",
            "infoEmpty": "Ничего не найдено",
            "infoFiltered": "(filtered from _MAX_ total records)"
        },
        "info":  false
    });
});
