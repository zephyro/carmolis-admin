<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>АРХИВ КОНКУРСОВ</span>
                                <sub class="color-blue">активные конкурсы</sub>
                            </h1>
                        </div>
                        <div class="heading__row_right">
                            <a href="#" class="btn">ДОБАВИТЬ КОНКУРС</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">
                    <div class="table_responsive">
	                    <table class="table">
		                    <tr>
			                    <th>Название</th>
			                    <th>Дата старта</th>
			                    <th>Дата окончания</th>
			                    <th>Участников</th>
			                    <th class="text-right">Подробней</th>
		                    </tr>
		                    <tr>
			                    <td>1-я презентация</td>
			                    <td>3 Jan 1985</td>
			                    <td>3 Jan 1985</td>
			                    <td>150</td>
			                    <td class="text-right">
				                    <a href="#" class="btn_next">
					                    <span>ДАЛЬШЕ</span>
					                    <i class="fa fa-angle-right"></i>
				                    </a>
			                    </td>
		                    </tr>
		                    <tr>
			                    <td>Викторина - выиграй миллион</td>
			                    <td>5 Dec 1986</td>
			                    <td>5 Dec 1986</td>
			                    <td>450</td>
			                    <td class="text-right">
				                    <a href="#" class="btn_next">
					                    <span>ДАЛЬШЕ</span>
					                    <i class="fa fa-angle-right"></i>
				                    </a>
			                    </td>
		                    </tr>
		                    <tr>
			                    <td>Marilyn Castro</td>
			                    <td>23 Oct 1956</td>
			                    <td>23 Oct 1956</td>
			                    <td>600</td>
			                    <td class="text-right">
				                    <a href="#" class="btn_next">
					                    <span>ДАЛЬШЕ</span>
					                    <i class="fa fa-angle-right"></i>
				                    </a>
			                    </td>
		                    </tr>
		                    <tr>
			                    <td>1-я презентация</td>
			                    <td>3 Jan 1985</td>
			                    <td>3 Jan 1985</td>
			                    <td>150</td>
			                    <td class="text-right">
				                    <a href="#" class="btn_next">
					                    <span>ДАЛЬШЕ</span>
					                    <i class="fa fa-angle-right"></i>
				                    </a>
			                    </td>
		                    </tr>
		                    <tr>
			                    <td>Викторина - выиграй миллион</td>
			                    <td>5 Dec 1986</td>
			                    <td>5 Dec 1986</td>
			                    <td>450</td>
			                    <td class="text-right">
				                    <a href="#" class="btn_next">
					                    <span>ДАЛЬШЕ</span>
					                    <i class="fa fa-angle-right"></i>
				                    </a>
			                    </td>
		                    </tr>
		                    <tr>
			                    <td>Marilyn Castro</td>
			                    <td>23 Oct 1956</td>
			                    <td>23 Oct 1956</td>
			                    <td>600</td>
			                    <td class="text-right">
				                    <a href="#" class="btn_next">
					                    <span>ДАЛЬШЕ</span>
					                    <i class="fa fa-angle-right"></i>
				                    </a>
			                    </td>
		                    </tr>
		                    <tr>
			                    <td>1-я презентация</td>
			                    <td>3 Jan 1985</td>
			                    <td>3 Jan 1985</td>
			                    <td>150</td>
			                    <td class="text-right">
				                    <a href="#" class="btn_next">
					                    <span>ДАЛЬШЕ</span>
					                    <i class="fa fa-angle-right"></i>
				                    </a>
			                    </td>
		                    </tr>
		                    <tr>
			                    <td>Викторина - выиграй миллион</td>
			                    <td>5 Dec 1986</td>
			                    <td>5 Dec 1986</td>
			                    <td>450</td>
			                    <td class="text-right">
				                    <a href="#" class="btn_next">
					                    <span>ДАЛЬШЕ</span>
					                    <i class="fa fa-angle-right"></i>
				                    </a>
			                    </td>
		                    </tr>
		                    <tr>
			                    <td>Marilyn Castro</td>
			                    <td>23 Oct 1956</td>
			                    <td>23 Oct 1956</td>
			                    <td>600</td>
			                    <td class="text-right">
				                    <a href="#" class="btn_next">
					                    <span>ДАЛЬШЕ</span>
					                    <i class="fa fa-angle-right"></i>
				                    </a>
			                    </td>
		                    </tr>
	                    </table>
                    </div>

	                <ul class="pagination">
		                <li><a href="#">1</a></li>
		                <li><a href="#">2</a></li>
		                <li><a href="#">3</a></li>
		                <li><span>...</span></li>
		                <li><a href="#">10</a></li>
		                <li><a href="#">11</a></li>
		                <li><a href="#">12</a></li>
	                </ul>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
