<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>новости</span>
                            </h1>
                        </div>
                        <div class="heading__row_right">
                            <a href="#" class="btn">ДОБАВИТЬ НОВОСТЬ</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">
                    <div class="table_responsive">
                        <table class="table">
                            <tr>
                                <th>Поз.</th>
                                <th>Название</th>
                                <th class="text-right">Подробней</th>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li></li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>1-я презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Викторина - выиграй миллион</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li></li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Marilyn Castro</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Jacqueline Thomas</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>1-я презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>1-я презентация</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Викторина - выиграй миллион</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Marilyn Castro</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="change_position">
                                        <li>
                                            <i class="fa fa-angle-up"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-angle-down"></i>
                                        </li>
                                    </ul>
                                </td>
                                <td>Jacqueline Thomas</td>
                                <td class="text-right">
                                    <a href="#" class="btn_next">
                                        <span>ДАЛЬШЕ</span>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
	                <ul class="pagination">
		                <li><a href="#">1</a></li>
		                <li><a href="#">2</a></li>
		                <li><a href="#">3</a></li>
		                <li><span>...</span></li>
		                <li><a href="#">10</a></li>
		                <li><a href="#">11</a></li>
		                <li><a href="#">12</a></li>
	                </ul>
                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->
    </body>
</html>
