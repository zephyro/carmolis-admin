<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>ПОЛЬЗОВАТЕЛИ</span>
                            </h1>
                        </div>
                        <div class="heading__row_right">
	                        <a class="heading__link" href="#">выгрузить базу в csv</a>
                            <a href="#" class="btn">ДОБАВИТЬ ПОЛЬЗОВАТЕЛЯ</a>
                        </div>
                    </div>
                </div>
            </div>

            <section class="main">
                <div class="container">
                    <div class="table_responsive">
	                    <table class="table data_table">
							<thead>
								<tr>
									<th>
										<span class="sort">Full Name</span>
									</th>
									<th>
										<span class="sort active">Email</span>
									</th>
									<th>
										<span class="sort active">Дата регистр.</span>
									</th>
									<th>
										<span class="sort">Баллы</span>
									</th>
									<th>
										<span class="sort">Статус</span>
									</th>
									<th></th>
								</tr>
							</thead>

		                    <tbody>


			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_01.jpg">
					                    </div>
					                    <span>Joyce Lynch</span>
				                    </td>
				                    <td><a class="link_black" href="#">joyce.lynch@company.com</a></td>
				                    <td>3 Jan 1985</td>
				                    <td>435</td>
				                    <td><span class="users_status users_status_green"></span> Ученик</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_02.png">
					                    </div>
					                    <span>Philip Little</span>
				                    </td>
				                    <td><a class="link_black" href="#">hellophilip@gmail.com</a></td>
				                    <td>5 Dec 1986</td>
				                    <td>432</td>
				                    <td><span class="users_status users_status_rose"></span> Эксперт+</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_03.jpg">
					                    </div>
					                    <span>Marilyn Castro</span>
				                    </td>
				                    <td><a class="link_black" href="#">castro.marilyn@microsoft.com</a></td>
				                    <td>23 Oct 1956</td>
				                    <td>4523</td>
				                    <td><span class="users_status users_status_yellow"></span> Эксперт</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_04.png">
					                    </div>
					                    <span>Jacqueline Thomas</span>
				                    </td>
				                    <td><a class="link_black" href="#">jacqueline.thomas@gmail.com</a></td>
				                    <td>2 Mar 1993</td>
				                    <td>423</td>
				                    <td><span class="users_status users_status_purple"></span> Специалист</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_02.png">
					                    </div>
					                    <span>Philip Little</span>
				                    </td>
				                    <td><a class="link_black" href="#">hellophilip@gmail.com</a></td>
				                    <td>5 Dec 1986</td>
				                    <td>432</td>
				                    <td><span class="users_status users_status_black"></span> Админ</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>



			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_01.jpg">
					                    </div>
					                    <span>Joyce Lynch</span>
				                    </td>
				                    <td><a class="link_black" href="#">joyce.lynch@company.com</a></td>
				                    <td>3 Jan 1985</td>
				                    <td>435</td>
				                    <td><span class="users_status users_status_green"></span> Ученик</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_02.png">
					                    </div>
					                    <span>Philip Little</span>
				                    </td>
				                    <td><a class="link_black" href="#">hellophilip@gmail.com</a></td>
				                    <td>5 Dec 1986</td>
				                    <td>432</td>
				                    <td><span class="users_status users_status_rose"></span> Эксперт+</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_03.jpg">
					                    </div>
					                    <span>Marilyn Castro</span>
				                    </td>
				                    <td><a class="link_black" href="#">castro.marilyn@microsoft.com</a></td>
				                    <td>23 Oct 1956</td>
				                    <td>4523</td>
				                    <td><span class="users_status users_status_yellow"></span> Эксперт</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_04.png">
					                    </div>
					                    <span>Jacqueline Thomas</span>
				                    </td>
				                    <td><a class="link_black" href="#">jacqueline.thomas@gmail.com</a></td>
				                    <td>2 Mar 1993</td>
				                    <td>423</td>
				                    <td><span class="users_status users_status_purple"></span> Специалист</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>


			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_01.jpg">
					                    </div>
					                    <span>Joyce Lynch</span>
				                    </td>
				                    <td><a class="link_black" href="#">joyce.lynch@company.com</a></td>
				                    <td>3 Jan 1985</td>
				                    <td>435</td>
				                    <td><span class="users_status users_status_green"></span> Ученик</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_02.png">
					                    </div>
					                    <span>Philip Little</span>
				                    </td>
				                    <td><a class="link_black" href="#">hellophilip@gmail.com</a></td>
				                    <td>5 Dec 1986</td>
				                    <td>432</td>
				                    <td><span class="users_status users_status_rose"></span> Эксперт+</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_03.jpg">
					                    </div>
					                    <span>Marilyn Castro</span>
				                    </td>
				                    <td><a class="link_black" href="#">castro.marilyn@microsoft.com</a></td>
				                    <td>23 Oct 1956</td>
				                    <td>4523</td>
				                    <td><span class="users_status users_status_yellow"></span> Эксперт</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_04.png">
					                    </div>
					                    <span>Jacqueline Thomas</span>
				                    </td>
				                    <td><a class="link_black" href="#">jacqueline.thomas@gmail.com</a></td>
				                    <td>2 Mar 1993</td>
				                    <td>423</td>
				                    <td><span class="users_status users_status_purple"></span> Специалист</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_02.png">
					                    </div>
					                    <span>Philip Little</span>
				                    </td>
				                    <td><a class="link_black" href="#">hellophilip@gmail.com</a></td>
				                    <td>5 Dec 1986</td>
				                    <td>432</td>
				                    <td><span class="users_status users_status_black"></span> Админ</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>



			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_01.jpg">
					                    </div>
					                    <span>Joyce Lynch</span>
				                    </td>
				                    <td><a class="link_black" href="#">joyce.lynch@company.com</a></td>
				                    <td>3 Jan 1985</td>
				                    <td>435</td>
				                    <td><span class="users_status users_status_green"></span> Ученик</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_02.png">
					                    </div>
					                    <span>Philip Little</span>
				                    </td>
				                    <td><a class="link_black" href="#">hellophilip@gmail.com</a></td>
				                    <td>5 Dec 1986</td>
				                    <td>432</td>
				                    <td><span class="users_status users_status_rose"></span> Эксперт+</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_03.jpg">
					                    </div>
					                    <span>Marilyn Castro</span>
				                    </td>
				                    <td><a class="link_black" href="#">castro.marilyn@microsoft.com</a></td>
				                    <td>23 Oct 1956</td>
				                    <td>4523</td>
				                    <td><span class="users_status users_status_yellow"></span> Эксперт</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_04.png">
					                    </div>
					                    <span>Jacqueline Thomas</span>
				                    </td>
				                    <td><a class="link_black" href="#">jacqueline.thomas@gmail.com</a></td>
				                    <td>2 Mar 1993</td>
				                    <td>423</td>
				                    <td><span class="users_status users_status_purple"></span> Специалист</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>


			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_01.jpg">
					                    </div>
					                    <span>Joyce Lynch</span>
				                    </td>
				                    <td><a class="link_black" href="#">joyce.lynch@company.com</a></td>
				                    <td>3 Jan 1985</td>
				                    <td>435</td>
				                    <td><span class="users_status users_status_green"></span> Ученик</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_02.png">
					                    </div>
					                    <span>Philip Little</span>
				                    </td>
				                    <td><a class="link_black" href="#">hellophilip@gmail.com</a></td>
				                    <td>5 Dec 1986</td>
				                    <td>432</td>
				                    <td><span class="users_status users_status_rose"></span> Эксперт+</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_03.jpg">
					                    </div>
					                    <span>Marilyn Castro</span>
				                    </td>
				                    <td><a class="link_black" href="#">castro.marilyn@microsoft.com</a></td>
				                    <td>23 Oct 1956</td>
				                    <td>4523</td>
				                    <td><span class="users_status users_status_yellow"></span> Эксперт</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_04.png">
					                    </div>
					                    <span>Jacqueline Thomas</span>
				                    </td>
				                    <td><a class="link_black" href="#">jacqueline.thomas@gmail.com</a></td>
				                    <td>2 Mar 1993</td>
				                    <td>423</td>
				                    <td><span class="users_status users_status_purple"></span> Специалист</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_02.png">
					                    </div>
					                    <span>Philip Little</span>
				                    </td>
				                    <td><a class="link_black" href="#">hellophilip@gmail.com</a></td>
				                    <td>5 Dec 1986</td>
				                    <td>432</td>
				                    <td><span class="users_status users_status_black"></span> Админ</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>



			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_01.jpg">
					                    </div>
					                    <span>Joyce Lynch</span>
				                    </td>
				                    <td><a class="link_black" href="#">joyce.lynch@company.com</a></td>
				                    <td>3 Jan 1985</td>
				                    <td>435</td>
				                    <td><span class="users_status users_status_green"></span> Ученик</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_02.png">
					                    </div>
					                    <span>Philip Little</span>
				                    </td>
				                    <td><a class="link_black" href="#">hellophilip@gmail.com</a></td>
				                    <td>5 Dec 1986</td>
				                    <td>432</td>
				                    <td><span class="users_status users_status_rose"></span> Эксперт+</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_03.jpg">
					                    </div>
					                    <span>Marilyn Castro</span>
				                    </td>
				                    <td><a class="link_black" href="#">castro.marilyn@microsoft.com</a></td>
				                    <td>23 Oct 1956</td>
				                    <td>4523</td>
				                    <td><span class="users_status users_status_yellow"></span> Эксперт</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <div class="users_avatar">
						                    <img src="images/user_04.png">
					                    </div>
					                    <span>Jacqueline Thomas</span>
				                    </td>
				                    <td><a class="link_black" href="#">jacqueline.thomas@gmail.com</a></td>
				                    <td>2 Mar 1993</td>
				                    <td>423</td>
				                    <td><span class="users_status users_status_purple"></span> Специалист</td>
				                    <td class="text-right">
					                    <a href="#" class="btn_next">
						                    <span>ДАЛЬШЕ</span>
						                    <i class="fa fa-angle-right"></i>
					                    </a>
				                    </td>
			                    </tr>

		                    </tbody>


	                    </table>
                    </div>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
