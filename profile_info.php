<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading heading_white">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>aлЕКСАНДР ПУШКОВ</span>
                            </h1>
                        </div>
                        <div class="heading__row_right">
	                        <a href="#" class="btn btn_border_rose">УДАЛИТЬ ПОЛЬЗОВАТЕЛЯ</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">

					<div class="profile">

						<div class="profile__nav">
							<ul>
								<li class="active"><a href="#">Общая информация</a></li>
								<li><a href="#">Баллы</a></li>
								<li><a href="#">Конкурс</a></li>
								<li><a href="#">История входа</a></li>
							</ul>
						</div>

						<div class="profile__content">

							<div class="row">
								<div class="col col-xs-12 col-lg-3 col-xl-3"></div>
								<div class="col col-xs-12 col-lg-9 col-xl-9">
									<div class="profile__heading">
										<h3>ОСНОВНАЯ ИНФОРМАЦИЯ</h3>
										<span class="profile__heading_value">156 баллов</span>
									</div>
								</div>
							</div>


							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">E-mail</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<div class="row">
											<div class="col col-xs-12 col-sm-6 col-md-5 col-lg-4 col-xl-4">
												<select class="form_select">
													<option value="">Подтвержден</option>
													<option value="">Не подтвержден</option>
												</select>
											</div>
											<div class="col col-xs-12 col-sm-6 col-md-7 col-lg-8 col-xl-8 center_box">
												<div class="center_box_inner">654686@gmail.com | регистрация - 11.04.2018, 11:43</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Имя и фамилия</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<input type="text" class="form_control" name="" value="Карвелис Александр" placeholder="">
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Фото пользователя</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<div class="profile__photo">
											<div class="profile__photo_item">
												<label class="photo_form">
													<input type="file" class="">
													<span>
														<img src="images/user_photo.jpg" class="img-fluid" alt="">
													</span>
												</label>
											</div>
											<div class="profile__photo_text">
												<a class="profile__photo_remove" href="#">удалить фото</a>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Привязанные аккаунты</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<ul class="profile__social">
											<li><a href="#">facebook.com/yuri.k</a></li>
											<li><a href="#">google.com/account5</a></li>
											<li><a href="#">facebook.com/yuri.k</a></li>
										</ul>
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Сертификат</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<div class="row">
											<div class="col col-xs-12 col-sm-6 col-md-5 col-lg-4 col-xl-4">
												<select class="form_select">
													<option value="">Подтвержден</option>
													<option value="">Не подтвержден</option>
												</select>
											</div>
											<div class="col col-xs-12 col-sm-6 col-md-7 col-lg-8 col-xl-8 center_box">
												<div class="center_box_inner"><a href="#">ссылка на изображение </a></div>
											</div>
										</div>
										<div class="pt-5">подтвержден dghek@gmail.com 11.08.2019,11:32</div>
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Тип пользователь</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<div class="row">
											<div class="col col-xs-12 col-sm-6 col-md-5 col-lg-4 col-xl-4">
												<select class="form_select">
													<option value="">Модератор</option>
													<option value="">Пользователь</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Кто пригласил</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9 center_box">
										<div class="center_box_inner"><a href="#">34304@gmail.com | Александр Пушков</a></div>
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Статус</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<span class="profile_status color-green">Новичок</span>
									</div>
								</div>
							</div>

							<div class="form_group text-right">
								<button type="submit" class="btn btn_blue btn_send">СОХРАНИТЬ</button>
							</div>

							<div class="profile_divider"></div>

							<div class="row">
								<div class="col col-xs-12 col-lg-4 col-xl-3"></div>
								<div class="col col-xs-12 col-lg-8 col-xl-9">
									<h3>Установить новый пароль пользователю</h3>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Пароль</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<input type="password" class="form_control" name="" value="" placeholder="***********">
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Повторите пароль</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<input type="password" class="form_control" name="" value="" placeholder="***********">
									</div>
								</div>
							</div>

							<div class="text-right">
								<button type="submit" class="btn btn_blue btn_send">СОХРАНИТЬ НОВЫЙ ПАРОЛЬ</button>
							</div>

						</div>

					</div>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
