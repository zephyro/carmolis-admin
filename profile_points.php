<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading heading_white">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>aлЕКСАНДР ПУШКОВ</span>
                            </h1>
                        </div>
                        <div class="heading__row_right">
	                        <a href="#" class="btn btn_border_rose">УДАЛИТЬ ПОЛЬЗОВАТЕЛЯ</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">

					<div class="profile">

						<div class="profile__nav">
							<ul>
								<li><a href="#">Общая информация</a></li>
								<li class="active"><a href="#">Баллы</a></li>
								<li><a href="#">Конкурс</a></li>
								<li><a href="#">История входа</a></li>
							</ul>
						</div>

						<div class="profile__content">

							<div class="row">
								<div class="col col-xs-12 col-lg-3 col-xl-3"></div>
								<div class="col col-xs-12 col-lg-9 col-xl-9">
									<div class="profile__heading">
										<h3>СПИСАТЬ БАЛЛЫ</h3>
										<span class="profile__heading_value">156 баллов</span>
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Сумма баллов</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<div class="row">
											<div class="col col-xs-12 col-sm-6 col-md-5 col-lg-4 col-xl-4">
												<input type="text" class="form_control" name="" value="156" placeholder="">
											</div>
											<div class="col col-xs-12 col-sm-6 col-md-7 col-lg-8 col-xl-8 center_box">
												<div class="center_box_inner">баллов</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Имя и фамилия</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<textarea class="form_control" name="" placeholder="" rows="7">Вы победили за месяц. Мы списали Ваши баллы. Обратитесь к нам по телефону:+132465798</textarea>
									</div>
								</div>
							</div>


							<div class="form_group text-right">
								<button type="submit" class="btn btn_blue btn_send">СПИСАТЬ И ОТПРАВИТЬ ПИСЬМО</button>
							</div>

							<div class="profile_divider"></div>

							<div class="row">
								<div class="col col-xs-12 col-lg-3 col-xl-3"></div>
								<div class="col col-xs-12 col-lg-9 col-xl-9">
									<h3>НАЧИСЛЕНИЕ И СПИСАНИЕ БАЛЛОВ</h3>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label color-green">Начислено</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<ul class="point_list">
											<li>12 баллов | 11.04.2018, 11:34 |  презентация </li>
											<li>12 баллов | 11.04.2018, 11:34 |  презентация </li>
											<li>12 баллов | 11.04.2018, 11:34 |  презентация </li>
											<li>12 баллов | 11.04.2018, 11:34 |  презентация </li>
											<li>12 баллов | 11.04.2018, 11:34 |  презентация </li>
										</ul>
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label color-rose">Списано</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<ul class="point_list">
											<li class="color-rose">156 баллов | 11.04.2018, 11:34 | alexx8843@gmail.com | презентация  Комментарий: победили в конкурсе</li>
										</ul>
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label color-green">Начислено</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<ul class="point_list">
											<li>12 баллов | 11.04.2018, 11:34 |  презентация </li>
											<li>12 баллов | 11.04.2018, 11:34 |  презентация </li>
											<li>12 баллов | 11.04.2018, 11:34 |  презентация </li>
											<li>12 баллов | 11.04.2018, 11:34 |  презентация </li>
											<li>12 баллов | 11.04.2018, 11:34 |  презентация </li>
										</ul>
									</div>
								</div>
							</div>

						</div>

					</div>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
