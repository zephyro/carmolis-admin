<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>

        <div class="auth_page">
	    <div class="auth">
		    <div class="auth_form">
			    <div class="auth_form_title">Вход в кабинет</div>
			    <form class="form">

				    <div class="form_group">
					    <div class="form_elem">
						    <input class="form_elem_control" type="text" name="email" placeholder="Ваш e-mail">
						    <div class="form_elem_icon icon_envelope">
							    <svg class="ico-svg"  viewBox="0 0 97 62" xmlns="http://www.w3.org/2000/svg">
								    <use xlink:href="img/sprite_icons.svg#icon_envelope" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
							    </svg>
						    </div>
					    </div>
				    </div>

				    <div class="form_group">
					    <div class="form_elem">
						    <input class="form_elem_control" type="password" name="password" placeholder="Пароль">
						    <div class="form_elem_icon icon_lock">
							    <svg class="ico-svg" viewBox="0 0 36 35" fill="none" xmlns="http://www.w3.org/2000/svg">
								    <use xlink:href="img/sprite_icons.svg#icon_lock" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
							    </svg>
						    </div>
					    </div>
				    </div>

				    <div class="form_group">
					    <ul class="form_meta">
						    <li>
							    <label class="form_checkbox">
								    <input type="checkbox" name="" value="1">
								    <span>запомнить меня</span>
							    </label>
						    </li>
						    <li>
							    <a href="#">забыли пароль?</a>
						    </li>
					    </ul>
				    </div>

				    <div class="auth_form_button">
					    <button class="btn btn_blue btn_large" type="submit">Войти</button>
				    </div>
			    </form>
		    </div>
	    </div>
    </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
