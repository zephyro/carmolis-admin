<header class="header">
    <div class="container">
        <a class="header__logo" href="/">
            <img src="img/header_logo.svg" class="img-fluid" alt="">
        </a>
        <nav class="header__nav">
            <ul>
                <li><a href="#"><span>МОДУЛЬ</span></a></li>
                <li><a href="#"><span>КОНКУРС</span></a></li>
                <li><a href="#"><span>ПОДТВЕРЖДЕНИЕ</span><i>35</i></a></li>
                <li><a href="#"><span>НОВОСТИ</span></a></li>
                <li><a href="#"><span>СТРАНИЦЫ</span></a></li>
                <li><a href="#"><span>ПОЛЬЗОВАТЕЛИ</span></a></li>
            </ul>
        </nav>
        <a href="#" class="header__toggle">
            <span></span>
            <span></span>
            <span></span>
        </a>
        <div class="header__settings">
            <a class="header__settings_button" href="#"></a>
            <div class="header__settings_dropdown">
                <ul>
                    <li><a href="#">Призы</a></li>
                    <li><a href="#">Отзывы</a></li>
                    <li><a href="#">Мои настройки</a></li>
                    <li><a href="#">Выход</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>