<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>Общие настройки</span>
                            </h1>
                        </div>
                        <div class="heading__row_right">

                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">

					<div class="profile">

						<div class="profile__nav">
							<ul>
								<li><a href="#">Призы</a></li>
								<li class="active"><a href="#">Отзывы</a></li>
							</ul>
						</div>

						<div class="profile__content">

							<div class="row">
								<div class="col col-xs-12 col-lg-3 col-xl-3"></div>
								<div class="col col-xs-12 col-lg-9 col-xl-9">
									<h3>УПРАВЛЕНИЕ ОТЗЫВАМИ</h3>
								</div>
							</div>

							<div class="prize">

								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3"></div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<div class="prize__heading">
											<div class="prize__heading_left">
												<h3>ОТЗЫВ 1</h3>
												<div class="prize__nav"><a class="prize__nav_up" href="#">вверх</a> | <a class="prize__nav_down" href="#">вниз</a></div>
											</div>
											<div class="prize__heading_right"><a class="prize__remove" href="#">удалить отзыв</a></div>
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Текст отзыва</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<textarea class="form_control" name="" placeholder="" rows="6">2 билета в театр</textarea>
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Ссылка вконтакте</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="" placeholder="">
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Ссылка facebook</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="" placeholder="">
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Ссылка odnoklassniki</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="" placeholder="">
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Ссылка instagram</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="" placeholder="">
										</div>
									</div>
								</div>

							</div>

							<div class="profile_divider"></div>

							<div class="prize">

								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3"></div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<div class="prize__heading">
											<div class="prize__heading_left">
												<h3>ОТЗЫВ 2</h3>
												<div class="prize__nav"><a class="prize__nav_up" href="#">вверх</a> | <a class="prize__nav_down" href="#">вниз</a></div>
											</div>
											<div class="prize__heading_right"><a class="prize__remove" href="#">удалить отзыв</a></div>
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Текст отзыва</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<textarea class="form_control" name="" placeholder="" rows="6">2 билета в театр</textarea>
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Ссылка вконтакте</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="" placeholder="">
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Ссылка facebook</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="" placeholder="">
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Ссылка odnoklassniki</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="" placeholder="">
										</div>
									</div>
								</div>

								<div class="form_group form_group_inline">
									<div class="row">
										<div class="col col-xs-12 col-lg-3 col-xl-3">
											<label class="form_label">Ссылка instagram</label>
										</div>
										<div class="col col-xs-12 col-lg-9 col-xl-9">
											<input type="text" class="form_control" name="" value="" placeholder="">
										</div>
									</div>
								</div>

							</div>

							<div class="profile_divider"></div>

							<div class="text-right">
								<button type="submit" class="btn btn_blue btn_send">ДОБАВИТЬ</button>
							</div>
						</div>

					</div>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
