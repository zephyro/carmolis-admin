<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>Добавление модуля</span>
                            </h1>
                        </div>
                        <div class="heading__row_right">
                            <a href="#" class="btn btn_border_rose">В АРХИВ</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">
          
                    <form class="form">
                        
                        <div class="form_box">
                            <div class="row">
                                <div class="col col-xs-12 col-xl-10 col-xl-offset-1">
                                    <div class="row">
                                        <div class="col col-xs-12 col-lg-6 col-lg-offset-6 col-xl-7 col-xl-offset-5">
                                            <h3>ОБЩИЕ НАСТРОЙКИ</h3>
                                        </div>
                                    </div>
                                    <div class="form_group form_group_inline">
                                        <div class="row">
                                            <div class="col col-xs-12 col-lg-6 col-xl-5">
                                                <label class="form_label">Название</label>
                                            </div>
                                            <div class="col col-xs-12 col-lg-6 col-xl-7">
                                                <input class="form_control" type="text" name="" placeholder="" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_group form_group_inline">
                                        <div class="row">
                                            <div class="col col-xs-12 col-lg-6 col-xl-5">
                                                <label class="form_label">Комментарии (видны только в админке)</label>
                                            </div>
                                            <div class="col col-xs-12 col-lg-6 col-xl-7">
                                                <input class="form_control" type="text" name="" placeholder="" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_group form_group_inline">
                                        <div class="row">
                                            <div class="col col-xs-12 col-lg-6 col-xl-5">
                                                <label class="form_label">Выберите тип теста</label>
                                            </div>
                                            <div class="col col-xs-12 col-lg-6 col-xl-7">
                                                <div>
                                                    <label class="form_radio">
                                                        <input type="radio" name="test" value="" checked>
                                                        <span>Викторина</span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <label class="form_radio">
                                                        <input type="radio" name="test" value="">
                                                        <span>Презентация</span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <label class="form_radio">
                                                        <input type="radio" name="test" value="">
                                                        <span>Тренежер</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form_box">
                            <div class="row">
                                <div class="col col-xs-12 col-xl-10 col-xl-offset-1">
                                    <div class="row">
                                        <div class="col col-xs-12 col-lg-6 col-lg-offset-6 col-xl-7 col-xl-offset-5">
                                            <h3>НАСТРОЙКИ ВИКТОРИНЫ</h3>
                                        </div>
                                    </div>

                                    <div class="form_group form_group_inline">
                                        <div class="row">
                                            <div class="col col-xs-12 col-lg-6 col-xl-5">
                                                <label class="form_label">Информация</label>
                                            </div>
                                            <div class="col col-xs-12 col-lg-6 col-xl-7">
                                                <div class="form_info">
                                                    Викторина идет без ограничения по времени, баллы начисляются в зависимости от ответов. Вопросы и ответы тестируемому выдаются в разброс
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="form_box">
                            <div class="row">
                                <div class="col col-xs-12 col-xl-10 col-xl-offset-1">
                                    <div class="row">
                                        <div class="col col-xs-12 col-lg-6 col-lg-offset-6 col-xl-7 col-xl-offset-5">
                                            <h3>ДОБАВЛЕНИЯ ТЕСТА ДЛЯ ВИКТОРИНЫ</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="new_question">
                                <div class="text-right">
                                    <a href="#" class="new_question__remove">Удалить вопрос</a>
                                </div>

                                <div class="form_group form_group_inline">
                                    <div class="row">
                                        <div class="col col-xs-12 col-md-3 col-lg-2 col-xl-2">
                                            <label class="form_label new_question__label">
                                                <span>Вопрос</span>
                                                <ul class="new_question__position">
                                                    <li><a href="#">Вверх</a></li>
                                                    <li><a href="#">Вниз</a></li>
                                                </ul>
                                            </label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-10 col-xl-10">
                                            <textarea class="form_control" name="" placeholder="" rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form_group form_group_inline">
                                    <div class="row mb-5">
                                        <div class="col col-xs-12 col-md-3 col-lg-4">
                                            <label class="form_label form_label_md">Ответ</label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-8">
                                            <div class="input_group">
                                                <input class="form_control" type="text" name="" placeholder="" value="">
                                                <div class="input_group_append">
                                                    <div class="input_group_label new_question__status"><span class="color-green">верный</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-xs-12 col-md-3 col-lg-4">
                                            <label class="form_label form_label_md">Сообщение</label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-8">
                                            <input class="form_control" type="text" name="" placeholder="" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form_group form_group_inline">
                                    <div class="row mb-5">
                                        <div class="col col-xs-12 col-md-3 col-lg-4">
                                            <label class="form_label form_label_md">Ответ</label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-8">
                                            <div class="input_group">
                                                <input class="form_control" type="text" name="" placeholder="" value="">
                                                <div class="input_group_append">
                                                    <div class="input_group_label new_question__status">нейтральный</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-xs-12 col-md-3 col-lg-4">
                                            <label class="form_label form_label_md">Сообщение</label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-8">
                                            <input class="form_control" type="text" name="" placeholder="" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form_group form_group_inline">
                                    <div class="row mb-5">
                                        <div class="col col-xs-12 col-md-3 col-lg-4">
                                            <label class="form_label form_label_md">Ответ</label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-8">
                                            <div class="input_group">
                                                <input class="form_control" type="text" name="" placeholder="" value="">
                                                <div class="input_group_append">
                                                    <div class="input_group_label new_question__status"><span class="color-red">неверный</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-xs-12 col-md-3 col-lg-4">
                                            <label class="form_label form_label_md">Сообщение</label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-8">
                                            <input class="form_control" type="text" name="" placeholder="" value="">
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="new_question">
                                <div class="text-right">
                                    <a href="#" class="new_question__remove">Удалить вопрос</a>
                                </div>

                                <div class="form_group form_group_inline">
                                    <div class="row">
                                        <div class="col col-xs-12 col-md-3 col-lg-2 col-xl-2">
                                            <label class="form_label new_question__label">
                                                <span>Вопрос</span>
                                                <ul class="new_question__position">
                                                    <li><a href="#">Вверх</a></li>
                                                    <li><a href="#">Вниз</a></li>
                                                </ul>
                                            </label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-10 col-xl-10">
                                            <textarea class="form_control" name="" placeholder="" rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form_group form_group_inline">
                                    <div class="row mb-5">
                                        <div class="col col-xs-12 col-md-3 col-lg-4">
                                            <label class="form_label form_label_md">Ответ</label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-8">
                                            <div class="input_group">
                                                <input class="form_control" type="text" name="" placeholder="" value="">
                                                <div class="input_group_append">
                                                    <div class="input_group_label new_question__status"><span class="color-green">верный</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-xs-12 col-md-3 col-lg-4">
                                            <label class="form_label form_label_md">Сообщение</label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-8">
                                            <input class="form_control" type="text" name="" placeholder="" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form_group form_group_inline">
                                    <div class="row mb-5">
                                        <div class="col col-xs-12 col-md-3 col-lg-4">
                                            <label class="form_label form_label_md">Ответ</label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-8">
                                            <div class="input_group">
                                                <input class="form_control" type="text" name="" placeholder="" value="">
                                                <div class="input_group_append">
                                                    <div class="input_group_label new_question__status">нейтральный</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-xs-12 col-md-3 col-lg-4">
                                            <label class="form_label form_label_md">Сообщение</label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-8">
                                            <input class="form_control" type="text" name="" placeholder="" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form_group form_group_inline">
                                    <div class="row mb-5">
                                        <div class="col col-xs-12 col-md-3 col-lg-4">
                                            <label class="form_label form_label_md">Ответ</label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-8">
                                            <div class="input_group">
                                                <input class="form_control" type="text" name="" placeholder="" value="">
                                                <div class="input_group_append">
                                                    <div class="input_group_label new_question__status"><span class="color-red">неверный</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-xs-12 col-md-3 col-lg-4">
                                            <label class="form_label form_label_md">Сообщение</label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-8">
                                            <input class="form_control" type="text" name="" placeholder="" value="">
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="new_question">
                                <div class="form_group text-right">
                                    <a href="#" class="btn btn_border_blue">ДОБАВИТЬ ВОПРОС</a>
                                </div>
                                <div class="form_group form_group_inline">
                                    <div class="row">
                                        <div class="col col-xs-12 col-md-3 col-lg-2 col-xl-2">
                                            <label class="form_label new_question__label">
                                                Последнее сообщение
                                            </label>
                                        </div>
                                        <div class="col col-xs-12 col-md-9 col-lg-10 col-xl-10">
                                            <textarea class="form_control" name="" placeholder="" rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn_blue btn_send">СОХРАНИТЬ И РАЗМЕСТИТЬ НА ПОРТАЛЕ</button>
                                </div>
                            </div>

                        </div>
                    
                    </form>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
