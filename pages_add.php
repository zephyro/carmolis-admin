<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>ДОБАВЛЕНИЕ страницы</span>
                            </h1>
                        </div>
                        <div class="heading__row_right">
                            <a href="#" class="btn btn_border_rose">УДАЛИТЬ</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">

	                <form class="form">

		                <div class="form_group form_group_inline">
			                <div class="row">
				                <div class="col col-xs-12 col-lg-3 col-xl-3">
					                <label class="form_label">Название</label>
				                </div>
				                <div class="col col-xs-12 col-lg-7 col-xl-7">
					                <input class="form_control" type="text" name="" placeholder="" value="">
				                </div>
			                </div>
		                </div>

		                <div class="form_group form_group_inline">
			                <div class="row">
				                <div class="col col-xs-12 col-lg-3 col-xl-3">
					                <label class="form_label">Адрес страницы</label>
				                </div>
				                <div class="col col-xs-12 col-lg-7 col-xl-7">
					                <input class="form_control" type="text" name="" placeholder="" value="https://carmolis.ru/doc4032" disabled>
				                </div>
			                </div>
		                </div>

		                <div class="form_group form_group_inline">
			                <div class="row">
				                <div class="col col-xs-12 col-lg-3 col-xl-3">
					                <label class="form_label">Изображение</label>
				                </div>
				                <div class="col col-xs-12 col-lg-7 col-xl-7">
					                <label class="form_file">
						                <input class="form_file__input" type="file" name="" placeholder="" value="">
						                <span class="form_file__icon">
						                <i class="fa fa-camera" aria-hidden="true"></i>
					                </span>
						                <span class="form_file__text"><span></span></span>
						                <span class="form_file__btn">Загрузить</span>
					                </label>
				                </div>
			                </div>
		                </div>

		                <div class="form_group form_group_inline">
			                <div class="row">
				                <div class="col col-xs-12 col-lg-3 col-xl-3">
					                <label class="form_label">Текст</label>
				                </div>
				                <div class="col col-xs-12 col-lg-9 col-xl-9">
					                <textarea class="form_control" name="" placeholder="" rows="7"></textarea>
				                </div>
			                </div>
		                </div>

		                <div class="text-right">
			                <button type="submit" class="btn btn_blue btn_send">СОХРАНИТЬ И РАЗМЕСТИТЬ НА ПОРТАЛЕ</button>
		                </div>

	                </form>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->
    </body>
</html>
