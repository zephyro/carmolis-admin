<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>добавление конкурса</span>
                            </h1>
                        </div>
                        <div class="heading__row_right">
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">

	                <form class="form">
		                <div class="row">
			                <div class="col col-xs-12 col-xl-10 col-xl-offset-1">

				                <div class="row">
					                <div class="col col-xs-12 col-lg-6 col-lg-offset-6 col-xl-7 col-xl-offset-5">
						                <h3>ОБЩИЕ НАСТРОЙКИ</h3>
					                </div>
				                </div>

				                <div class="form_group form_group_inline">
					                <div class="row">
						                <div class="col col-xs-12 col-lg-6 col-xl-5">
							                <label class="form_label">Название</label>
						                </div>
						                <div class="col col-xs-12 col-lg-6 col-xl-7">
							                <input class="form_control" type="text" name="" placeholder="" value="">
						                </div>
					                </div>
				                </div>

				                <div class="form_group form_group_inline">
					                <div class="row">
						                <div class="col col-xs-12 col-lg-6 col-xl-5">
							                <label class="form_label">Начала проведения конкурса</label>
						                </div>
						                <div class="col col-xs-6 col-sm-4 col-lg-3 col-xl-2">
							                <input class="form_control form_date" type="text" name="" placeholder="" value="05.08.2019">
						                </div>
					                </div>
				                </div>

				                <div class="form_group form_group_inline">
					                <div class="row">
						                <div class="col col-xs-12 col-lg-6 col-xl-5">
							                <label class="form_label">Начала проведения конкурса</label>
						                </div>
						                <div class="col col-xs-6 col-sm-4 col-lg-3 col-xl-2">
							                <input class="form_control form_date" type="text" name="" placeholder="" value="05.08.2019">
						                </div>
					                </div>
				                </div>
				                <br/>
				                <br/>
				                <div class="text-right">
					                <button type="submit" class="btn btn_blue btn_send">СОХРАНИТЬ И РАЗМЕСТИТЬ НА ПОРТАЛЕ</button>
				                </div>

			                </div>
		                </div>
	                </form>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
