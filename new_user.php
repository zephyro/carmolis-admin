<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading heading_white">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>Добавление пользователя</span>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">

					<div class="profile">

						<div class="profile__nav">
							<ul>
								<li class="active"><a href="#">Новый пользователь</a></li>
							</ul>
						</div>

						<div class="profile__content">

							<div class="row">
								<div class="col col-xs-12 col-lg-3 col-xl-3"></div>
								<div class="col col-xs-12 col-lg-9 col-xl-9">
									<h3>ДОБАВЛЕНИЕ ПОЛЬЗОВАТЕЛЯ</h3>
								</div>
							</div>


							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">E-mail</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<input type="text" class="form_control" name="" value="" placeholder="">
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Пароль</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<input type="password" class="form_control" name="" value="" placeholder="***********">
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Повторите пароль</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<input type="password" class="form_control" name="" value="" placeholder="***********">
									</div>
								</div>
							</div>

							<div class="form_group form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-6 col-xl-5">
										<label class="form_label">Тип пользователя</label>
									</div>
									<div class="col col-xs-12 col-lg-6 col-xl-7">
										<div>
											<label class="form_radio">
												<input type="radio" name="type" value="">
												<span>Админ</span>
											</label>
										</div>
										<div>
											<label class="form_radio">
												<input type="radio" name="type" value="" checked>
												<span>Модератор</span>
											</label>
										</div>
										<div>
											<label class="form_radio">
												<input type="radio" name="type" value="">
												<span>Обычный пользователь</span>
											</label>
										</div>
									</div>
								</div>
							</div>

							<div class="text-right">
								<button type="submit" class="btn btn_blue btn_send">ДОБАВИТЬ ПОЛЬЗОВАТЕЛЯ</button>
							</div>

						</div>

					</div>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
