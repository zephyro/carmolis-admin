<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>кОНКУРС “лЕТО 2020 ГОДА”</span>
                            </h1>
                        </div>
                        <div class="heading__row_right">
	                        <a href="#" class="btn">ИЗМЕНИТЬ НАСТРОЙКИ КОНКУРСА</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">

	                <h3>УЧАСТНИКОВ 400</h3>

	                <ul class="users_list">
		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>
		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>
		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>

		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>
		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>
		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>

		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>
		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>
		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>

		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>
		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>
		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>

		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>
		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>
		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>

		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>
		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>
		                <li><a href="#">Александр Пушков</a></li>
		                <li><a href="#">Александр Пушков Мл...</a></li>
	                </ul>

	                <h3>Работы</h3>

	                <ul class="users_work">
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>300+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>60+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>120+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>80+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>400+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>600+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>300+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>60+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>120+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>80+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>400+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>600+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>300+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>60+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>120+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>80+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>400+</span>
			                </a>
		                </li>
		                <li>
			                <a href="#">
				                <img src="images/no_image.jpg" class="img-fluid" alt="">
				                <span>600+</span>
			                </a>
		                </li>
	                </ul>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
