<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>тРЕБУЕТСЯ ПОДТВЕРЖДЕНИЕ</span>
                            </h1>
	                        <ul class="heading__filter">
		                        <li class="active"><a href="#">ВСЕ (35)</a></li>
		                        <li><a href="#">ОТЗЫВЫ (15)</a></li>
		                        <li><a href="#">СЕРТИФИКАТ(1)</a></li>
		                        <li><a href="#">КОНКУРСЫ (14)</a></li>
		                        <li><a href="#">ВОПРОСЫ (10)</a></li>
	                        </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">

	                <div class="confirmation">

		                <ul>
			                <li>
				                <div class="confirmation__photo">
					                <img src="images/image.png" class="img-fluid" alt="">
				                </div>
			                </li>
			                <li>
				                <a href="mailto:joyce.lynch@company.com" class="confirmation__email">joyce.lynch@company.com</a>
			                </li>
			                <li>
				                <textarea class="form_control" vocab="" name="" rows="3">Ваша фотография не </textarea>
			                </li>
			                <li>
				                <ul class="confirmation__buttons">
					                <li>
						                <a href="#" class="btn btn_border_rose btn_square btn_sm">ОТКАЗАТЬ </a>
					                </li>
					                <li>
						                <button type="submit" class="btn btn_blue btn_sm">Подтвердить сертификат</button>
					                </li>
				                </ul>
			                </li>
		                </ul>

		                <ul>
			                <li>
				                <div class="confirmation__photo">
					                <img src="images/image.png" class="img-fluid" alt="">
				                </div>
			                </li>
			                <li>
				                <a href="mailto:joyce.lynch@company.com" class="confirmation__email">joyce.lynch@company.com</a>
			                </li>
			                <li>
				                <textarea class="form_control" vocab="" name="" rows="3">Ваша фотография не </textarea>
			                </li>
			                <li>
				                <ul class="confirmation__buttons">
					                <li>
						                <a href="#" class="btn btn_border_rose btn_square btn_sm">ОТКАЗАТЬ </a>
					                </li>
					                <li>
						                <button type="submit" class="btn btn_blue btn_sm">Подтвердить фото для конкура</button>
					                </li>
				                </ul>
			                </li>
		                </ul>

		                <ul>
			                <li>
				                <div class="confirmation__photo">
					                <img src="images/image.png" class="img-fluid" alt="">
				                </div>
			                </li>
			                <li>
				                <a href="mailto:joyce.lynch@company.com" class="confirmation__email">joyce.lynch@company.com</a>
			                </li>
			                <li>
				                <textarea class="form_control" vocab="" name="" rows="3">Ваша фотография не </textarea>
			                </li>
			                <li>
				                <ul class="confirmation__buttons">
					                <li>
						                <a href="#" class="btn btn_border_rose btn_square btn_sm">Удалить отзыв</a>
					                </li>
					                <li>
						                <button type="submit" class="btn btn_blue btn_sm">Отправить ответ на вопрос</button>
					                </li>
				                </ul>
			                </li>
		                </ul>

		                <ul>
			                <li>
				                <div class="confirmation__photo">
					                <img src="images/image.png" class="img-fluid" alt="">
				                </div>
			                </li>
			                <li>
				                <a href="mailto:joyce.lynch@company.com" class="confirmation__email">joyce.lynch@company.com</a>
			                </li>
			                <li>
				                <textarea class="form_control" vocab="" name="" rows="3">Ваша фотография не </textarea>
			                </li>
			                <li>
				                <ul class="confirmation__buttons">
					                <li>
						                <a href="#" class="btn btn_border_rose btn_square btn_sm">ОТКАЗАТЬ </a>
					                </li>
					                <li>
						                <button type="submit" class="btn btn_blue btn_sm">Подтвердить сертификат</button>
					                </li>
				                </ul>
			                </li>
		                </ul>

	                </div>


                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
