<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading heading_white">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>aлЕКСАНДР ПУШКОВ</span>
                            </h1>
                        </div>
                        <div class="heading__row_right">
	                        <a href="#" class="btn btn_border_rose">УДАЛИТЬ ПОЛЬЗОВАТЕЛЯ</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">

					<div class="profile">

						<div class="profile__nav">
							<ul>
								<li><a href="#">Общая информация</a></li>
								<li><a href="#">Баллы</a></li>
								<li><a href="#">Конкурс</a></li>
								<li class="active"><a href="#">История входа</a></li>
							</ul>
						</div>

						<div class="profile__content">

							<div class="row">
								<div class="col col-xs-12 col-lg-3 col-xl-3"></div>
								<div class="col col-xs-12 col-lg-9 col-xl-9">
									<h3>ДАТА И ВРЕМЯ ВХОДА</h3>
								</div>
							</div>

							<div class="form_group_inline">
								<div class="row">
									<div class="col col-xs-12 col-lg-3 col-xl-3">
										<label class="form_label">Данные входа</label>
									</div>
									<div class="col col-xs-12 col-lg-9 col-xl-9">
										<ul class="point_list">
											<li>11.04.2018, 11:34 |  156.11.23.29 </li>
											<li>11.04.2018, 11:34 |  156.11.23.29 </li>
											<li>11.04.2018, 11:34 |  156.11.23.29 </li>
											<li>11.04.2018, 11:34 |  156.11.23.29 </li>
											<li>11.04.2018, 11:34 |  156.11.23.29 </li>
										</ul>
									</div>
								</div>
							</div>


						</div>

					</div>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
