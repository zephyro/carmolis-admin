<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="heading">
                <div class="container">
                    <div class="heading__row">
                        <div class="heading__row_left">
                            <h1>
                                <span>Добавление модуля</span>
                            </h1>
                        </div>
                        <div class="heading__row_right">
                            <a href="#" class="btn btn_border_rose">В АРХИВ</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <section class="main">
                <div class="container">
          
                    <form class="form">
                        
                        <div class="form_box">
                            <div class="row">
                                <div class="col col-xs-12 col-xl-10 col-xl-offset-1">
                                    <div class="row">
                                        <div class="col col-xs-12 col-lg-6 col-lg-offset-6 col-xl-7 col-xl-offset-5">
                                            <h3>Общие настройки МОДУЛЯ</h3>
                                        </div>
                                    </div>
                                    <div class="form_group form_group_inline">
                                        <div class="row">
                                            <div class="col col-xs-12 col-lg-6 col-xl-5">
                                                <label class="form_label">Название</label>
                                            </div>
                                            <div class="col col-xs-12 col-lg-6 col-xl-7">
                                                <input class="form_control" type="text" name="" placeholder="" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_group form_group_inline">
                                        <div class="row">
                                            <div class="col col-xs-12 col-lg-6 col-xl-5">
                                                <label class="form_label">Комментарии (видны только в админке)</label>
                                            </div>
                                            <div class="col col-xs-12 col-lg-6 col-xl-7">
                                                <input class="form_control" type="text" name="" placeholder="" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_group form_group_inline">
                                        <div class="row">
                                            <div class="col col-xs-12 col-lg-6 col-xl-5">
                                                <label class="form_label">Привязать к статусу</label>
                                            </div>
                                            <div class="col col-xs-12 col-lg-6 col-xl-7">
                                                <select class="form_select">
                                                    <option value="">ученик</option>
                                                    <option value="">Выпускник</option>
                                                    <option value="">Тренер</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_group form_group_inline">
                                        <div class="row">
                                            <div class="col col-xs-12 col-lg-6 col-xl-5">
                                                <label class="form_label">Выберите тип теста</label>
                                            </div>
                                            <div class="col col-xs-12 col-lg-6 col-xl-7">
	                                            <div class="test_change">
		                                            <div>
			                                            <label class="form_radio">
				                                            <input type="radio" name="test_type" value="1" checked>
				                                            <span>Викторина</span>
			                                            </label>
		                                            </div>
		                                            <div>
			                                            <label class="form_radio">
				                                            <input type="radio" name="test_type" value="2">
				                                            <span>Презентация</span>
			                                            </label>
		                                            </div>
		                                            <div>
			                                            <label class="form_radio">
				                                            <input type="radio" name="test_type" value="3">
				                                            <span>Тренежер</span>
			                                            </label>
		                                            </div>
	                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

	                    <div class="test_type type1 active">

		                    <div class="form_box">
			                    <div class="row">
				                    <div class="col col-xs-12 col-xl-10 col-xl-offset-1">
					                    <div class="row">
						                    <div class="col col-xs-12 col-lg-6 col-lg-offset-6 col-xl-7 col-xl-offset-5">
							                    <h3>НАСТРОЙКИ ВИКТОРИНЫ</h3>
						                    </div>
					                    </div>

					                    <div class="form_group form_group_inline">
						                    <div class="row">
							                    <div class="col col-xs-12 col-lg-6 col-xl-5">
								                    <label class="form_label">Информация</label>
							                    </div>
							                    <div class="col col-xs-12 col-lg-6 col-xl-7">
								                    <div class="form_info">
									                    Викторина идет без ограничения по времени, баллы начисляются в зависимости от ответов. Вопросы и ответы тестируемому выдаются в разброс
								                    </div>
							                    </div>
						                    </div>
					                    </div>

				                    </div>
			                    </div>
		                    </div>

		                    <div class="form_box">
			                    <div class="row">
				                    <div class="col col-xs-12 col-xl-10 col-xl-offset-1">
					                    <div class="row">
						                    <div class="col col-xs-12 col-lg-6 col-lg-offset-6 col-xl-7 col-xl-offset-5">
							                    <h3>ДОБАВЛЕНИЯ ТЕСТА ДЛЯ ВИКТОРИНЫ</h3>
						                    </div>
					                    </div>
				                    </div>
			                    </div>

			                    <div class="new_question">
				                    <div class="text-right">
					                    <a href="#" class="new_question__remove">Удалить вопрос</a>
				                    </div>

				                    <div class="form_group form_group_inline">

					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-2 col-xl-2">
							                    <label class="form_label new_question__label">
								                    <span>Вопрос</span>
								                    <ul class="new_question__position">
									                    <li><a href="#">Вниз</a></li>
								                    </ul>
							                    </label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-10 col-xl-10">
							                    <textarea class="form_control" name="" placeholder="" rows="4"></textarea>
						                    </div>
					                    </div>

					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-2 col-xl-2">
							                    <label class="form_label form_label_md">Фото</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-10 col-xl-10">
							                    <div class="row">
								                    <div class="col col-xs-12 col-md-10 col-lg-10 col-xl-11">
									                    <label class="form_file">
										                    <input class="form_file__input" type="file" name="" placeholder="" value="">
										                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
										                    <span class="form_file__text"><span></span></span>
										                    <span class="form_file__btn">Загрузить</span>
									                    </label>
								                    </div>
								                    <div class="col col-xs-12 col-md-2 col-lg-2 col-xl-1">
									                    <div class="image_manager"><a href="#">ссылка</a></div>
								                    </div>
							                    </div>
						                    </div>
					                    </div>
				                    </div>


				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-3 col-xl-3">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-9 col-xl-9">
							                    <div class="input_group">
								                    <input class="form_control" type="text" name="" placeholder="" value="">
								                    <div class="input_group_append">
									                    <select class="form_select" name="">
										                    <option value="">25 баллов</option>
										                    <option value="">50 баллов</option>
										                    <option value="">75 баллов</option>
										                    <option value="">100 баллов</option>
									                    </select>
								                    </div>
							                    </div>
						                    </div>
					                    </div>

					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-3 col-xl-3">
							                    <label class="form_label form_label_md">Фото</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-9 col-xl-9">
							                    <div class="row">
								                    <div class="col col-xs-12 col-md-10 col-lg-10 col-xl-10">
									                    <label class="form_file">
										                    <input class="form_file__input" type="file" name="" placeholder="" value="">
										                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
										                    <span class="form_file__text"><span></span></span>
										                    <span class="form_file__btn">Загрузить</span>
									                    </label>
								                    </div>
								                    <div class="col col-xs-12 col-md-2 col-lg-2 col-xl-2">
									                    <div class="image_manager"><a href="#">ссылка</a></div>
								                    </div>
							                    </div>
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-3 col-xl-3">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-9 col-xl-9">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-3 col-xl-3">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-9 col-xl-9">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-3 col-xl-3">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-9 col-xl-9">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

			                    </div>

			                    <div class="new_question">
				                    <div class="text-right">
					                    <a href="#" class="new_question__remove">Удалить вопрос</a>
				                    </div>

				                    <div class="form_group form_group_inline">

					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-2 col-xl-2">
							                    <label class="form_label new_question__label">
								                    <span>Вопрос</span>
								                    <ul class="new_question__position">
									                    <li><a href="#">Вверх</a></li>
									                    <li><a href="#">Вниз</a></li>
								                    </ul>
							                    </label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-10 col-xl-10">
							                    <textarea class="form_control" name="" placeholder="" rows="4"></textarea>
						                    </div>
					                    </div>

					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-2 col-xl-2">
							                    <label class="form_label form_label_md">Фото</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-10 col-xl-10">
							                    <div class="row">
								                    <div class="col col-xs-12 col-md-10 col-lg-10 col-xl-11">
									                    <label class="form_file">
										                    <input class="form_file__input" type="file" name="" placeholder="" value="">
										                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
										                    <span class="form_file__text"><span></span></span>
										                    <span class="form_file__btn">Загрузить</span>
									                    </label>
								                    </div>
								                    <div class="col col-xs-12 col-md-2 col-lg-2 col-xl-1">
									                    <div class="image_manager"><a href="#">ссылка</a></div>
								                    </div>
							                    </div>
						                    </div>
					                    </div>
				                    </div>


				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-3 col-xl-3">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-9 col-xl-9">
							                    <div class="input_group">
								                    <input class="form_control" type="text" name="" placeholder="" value="">
								                    <div class="input_group_append">
									                    <select class="form_select" name="">
										                    <option value="">25 баллов</option>
										                    <option value="">50 баллов</option>
										                    <option value="">75 баллов</option>
										                    <option value="">100 баллов</option>
									                    </select>
								                    </div>
							                    </div>
						                    </div>
					                    </div>

					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-3 col-xl-3">
							                    <label class="form_label form_label_md">Фото</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-9 col-xl-9">
							                    <div class="row">
								                    <div class="col col-xs-12 col-md-10 col-lg-10 col-xl-10">
									                    <label class="form_file">
										                    <input class="form_file__input" type="file" name="" placeholder="" value="">
										                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
										                    <span class="form_file__text"><span></span></span>
										                    <span class="form_file__btn">Загрузить</span>
									                    </label>
								                    </div>
								                    <div class="col col-xs-12 col-md-2 col-lg-2 col-xl-2">
									                    <div class="image_manager"><a href="#">ссылка</a></div>
								                    </div>
							                    </div>
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-3 col-xl-3">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-9 col-xl-9">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-3 col-xl-3">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-9 col-xl-9">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-3 col-xl-3">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-9 col-xl-9">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

			                    </div>

			                    <div class="new_question">
				                    <div class="text-right">
					                    <a href="#" class="btn btn_border_blue">ДОБАВИТЬ ВОПРОС</a>
				                    </div>
			                    </div>

			                    <div class="new_question">
				                    <div class="form_group form_group_inline">
					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-2 col-xl-2">
							                    <label class="form_label new_question__label">
								                    Последнее сообщение
							                    </label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-10 col-xl-10">
							                    <textarea class="form_control" name="" placeholder="" rows="4"></textarea>
						                    </div>
					                    </div>
				                    </div>
				                    <div class="text-right">
					                    <button type="submit" class="btn btn_blue btn_send">СОХРАНИТЬ И РАЗМЕСТИТЬ НА ПОРТАЛЕ</button>
				                    </div>
			                    </div>

		                    </div>


	                    </div>

	                    <div class="test_type type2">

		                    <div class="form_box">
			                    <div class="row">
				                    <div class="col col-xs-12 col-lg-8 col-lg-offset-4 col-xl-8 col-xl-offset-4">
					                    <h3>НАСТРОЙКИ ПРЕЗЕНТАЦИИ</h3>
				                    </div>
			                    </div>

			                    <div class="form_group form_group_inline">
				                    <div class="row">
					                    <div class="col col-xs-12 col-lg-4 col-xl-4">
						                    <label class="form_label">Информация</label>
					                    </div>
					                    <div class="col col-xs-12 col-lg-8 col-xl-8">
						                    <div class="form_info">
							                    Викторина идет без ограничения по времени, баллы начисляются в зависимости от ответов. Вопросы и ответы тестируемому выдаются в разброс
						                    </div>
					                    </div>
				                    </div>
			                    </div>
		                    </div>

		                    <div class="form_box">
			                    <div class="row">
				                    <div class="col col-xs-12 col-lg-8 col-lg-offset-4 col-xl-8 col-xl-offset-4">
					                    <h4>Добавление презентации для версии ПК</h4>
				                    </div>
			                    </div>

			                    <div class="form_group form_group_inline">
				                    <div class="row">
					                    <div class="col col-xs-12col-lg-4 col-xl-4">
						                    <label class="form_label">1-е изображение</label>
					                    </div>
					                    <div class="col col-xs-12 col-lg-8 col-xl-8">
						                    <div class="row">
							                    <div class="col col-xs-12 col-md-10 col-lg-10 col-xl-10">
								                    <label class="form_file">
									                    <input class="form_file__input" type="file" name="" placeholder="" value="">
									                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
									                    <span class="form_file__text"><span></span></span>
									                    <span class="form_file__btn">Загрузить</span>
								                    </label>
							                    </div>
							                    <div class="col col-xs-12 col-md-2 col-lg-2 col-xl-2">

							                    </div>
						                    </div>
					                    </div>
				                    </div>
			                    </div>

			                    <div class="form_group form_group_inline">
				                    <div class="row">
					                    <div class="col col-xs-12col-lg-4 col-xl-4">
						                    <label class="form_label">2-е изображение</label>
					                    </div>
					                    <div class="col col-xs-12 col-lg-8 col-xl-8">
						                    <div class="row">
							                    <div class="col col-xs-12 col-md-10 col-lg-10 col-xl-10">
								                    <label class="form_file">
									                    <input class="form_file__input" type="file" name="" placeholder="" value="">
									                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
									                    <span class="form_file__text"><span></span></span>
									                    <span class="form_file__btn">Загрузить</span>
								                    </label>
							                    </div>
							                    <div class="col col-xs-12 col-md-2 col-lg-2 col-xl-2">
								                    <div class="image_manager"><a href="#"><span class="color-rose">удалить</span></a></div>
							                    </div>
						                    </div>
					                    </div>
				                    </div>
			                    </div>

			                    <div class="form_group form_group_inline">
				                    <div class="row">
					                    <div class="col col-xs-12col-lg-4 col-xl-4">
						                    <label class="form_label">3-е изображение</label>
					                    </div>
					                    <div class="col col-xs-12 col-lg-8 col-xl-8">
						                    <div class="row">
							                    <div class="col col-xs-12 col-md-10 col-lg-10 col-xl-10">
								                    <label class="form_file">
									                    <input class="form_file__input" type="file" name="" placeholder="" value="">
									                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
									                    <span class="form_file__text"><span></span></span>
									                    <span class="form_file__btn">Загрузить</span>
								                    </label>
							                    </div>
							                    <div class="col col-xs-12 col-md-2 col-lg-2 col-xl-2">
								                    <div class="image_manager"><a href="#">Добавить</a></div>
							                    </div>
						                    </div>
					                    </div>
				                    </div>
			                    </div>

		                    </div>

		                    <div class="form_box">
			                    <div class="row">
				                    <div class="col col-xs-12 col-lg-8 col-lg-offset-4">
					                    <h4>Добавление презентации для мобильной версии</h4>
				                    </div>
			                    </div>

			                    <div class="form_group form_group_inline">
				                    <div class="row">
					                    <div class="col col-xs-12col-lg-4 col-xl-4">
						                    <label class="form_label">1-е изображение</label>
					                    </div>
					                    <div class="col col-xs-12 col-lg-8 col-xl-8">
						                    <div class="row">
							                    <div class="col col-xs-12 col-md-10 col-lg-10 col-xl-10">
								                    <label class="form_file">
									                    <input class="form_file__input" type="file" name="" placeholder="" value="">
									                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
									                    <span class="form_file__text"><span></span></span>
									                    <span class="form_file__btn">Загрузить</span>
								                    </label>
							                    </div>
							                    <div class="col col-xs-12 col-md-2 col-lg-2 col-xl-2">

							                    </div>
						                    </div>
					                    </div>
				                    </div>
			                    </div>

			                    <div class="form_group form_group_inline">
				                    <div class="row">
					                    <div class="col col-xs-12col-lg-4 col-xl-4">
						                    <label class="form_label">2-е изображение</label>
					                    </div>
					                    <div class="col col-xs-12 col-lg-8 col-xl-8">
						                    <div class="row">
							                    <div class="col col-xs-12 col-md-10 col-lg-10 col-xl-10">
								                    <label class="form_file">
									                    <input class="form_file__input" type="file" name="" placeholder="" value="">
									                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
									                    <span class="form_file__text"><span></span></span>
									                    <span class="form_file__btn">Загрузить</span>
								                    </label>
							                    </div>
							                    <div class="col col-xs-12 col-md-2 col-lg-2 col-xl-2">
								                    <div class="image_manager"><a href="#"><span class="color-rose">удалить</span></a></div>
							                    </div>
						                    </div>
					                    </div>
				                    </div>
			                    </div>

			                    <div class="form_group form_group_inline">
				                    <div class="row">
					                    <div class="col col-xs-12col-lg-4 col-xl-4">
						                    <label class="form_label">3-е изображение</label>
					                    </div>
					                    <div class="col col-xs-12 col-lg-8 col-xl-8">
						                    <div class="row">
							                    <div class="col col-xs-12 col-md-10 col-lg-10 col-xl-10">
								                    <label class="form_file">
									                    <input class="form_file__input" type="file" name="" placeholder="" value="">
									                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
									                    <span class="form_file__text"><span></span></span>
									                    <span class="form_file__btn">Загрузить</span>
								                    </label>
							                    </div>
							                    <div class="col col-xs-12 col-md-2 col-lg-2 col-xl-2">
								                    <div class="image_manager"><a href="#">Добавить</a></div>
							                    </div>
						                    </div>
					                    </div>
				                    </div>
			                    </div>

		                    </div>

		                    <div class="form_box">
			                    <div class="new_question">
				                    <div class="form_group form_group_inline">
					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-2 col-xl-2">
							                    <label class="form_label new_question__label">
								                    Последнее сообщение
							                    </label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-10 col-xl-10">
							                    <textarea class="form_control" name="" placeholder="" rows="4"></textarea>
						                    </div>
					                    </div>
				                    </div>
				                    <div class="text-right">
					                    <button type="submit" class="btn btn_blue btn_send">СОХРАНИТЬ И РАЗМЕСТИТЬ НА ПОРТАЛЕ</button>
				                    </div>
			                    </div>

		                    </div>

	                    </div>

	                    <div class="test_type type3">

		                    <div class="form_box">
			                    <div class="row">
				                    <div class="col col-xs-12 col-xl-10 col-xl-offset-1">
					                    <div class="row">
						                    <div class="col col-xs-12 col-lg-6 col-lg-offset-6 col-xl-7 col-xl-offset-5">
							                    <h3>НАСТРОЙКИ ТРЕНАЖЕРА</h3>
						                    </div>
					                    </div>

					                    <div class="form_group form_group_inline">
						                    <div class="row">
							                    <div class="col col-xs-12 col-lg-6 col-xl-5">
								                    <label class="form_label">Настройки баллов</label>
							                    </div>
							                    <div class="col col-xs-12 col-lg-6 col-xl-7">
								                    <div class="row mb-5">
									                    <div class="col col-xs-6">
										                    <select class="form_select">
											                    <option value="">до 5 мин</option>
											                    <option value="">до 10 мин</option>
											                    <option value="">до 20 мин</option>
										                    </select>
									                    </div>
									                    <div class="col col-xs-6">
										                    <select class="form_select">
											                    <option value="">75 баллов</option>
											                    <option value="">60 баллов</option>
											                    <option value="">45 баллов</option>
										                    </select>
									                    </div>
								                    </div>
								                    <div class="row mb-5">
									                    <div class="col col-xs-6">
										                    <select class="form_select">
											                    <option value="">до 5 мин</option>
											                    <option value="">до 10 мин</option>
											                    <option value="">до 20 мин</option>
										                    </select>
									                    </div>
									                    <div class="col col-xs-6">
										                    <select class="form_select">
											                    <option value="">75 баллов</option>
											                    <option value="">60 баллов</option>
											                    <option value="">45 баллов</option>
										                    </select>
									                    </div>
								                    </div>
								                    <div class="row mb-5">
									                    <div class="col col-xs-6">
										                    <select class="form_select">
											                    <option value="">до 5 мин</option>
											                    <option value="">до 10 мин</option>
											                    <option value="">до 20 мин</option>
										                    </select>
									                    </div>
									                    <div class="col col-xs-6">
										                    <select class="form_select">
											                    <option value="">75 баллов</option>
											                    <option value="">60 баллов</option>
											                    <option value="">45 баллов</option>
										                    </select>
									                    </div>
								                    </div>
							                    </div>
						                    </div>
					                    </div>

					                    <div class="form_group form_group_inline">
						                    <div class="row">
							                    <div class="col col-xs-12 col-lg-6 col-xl-5">
								                    <label class="form_label">Фоновое изображение</label>
							                    </div>
							                    <div class="col col-xs-12 col-lg-6 col-xl-7">
								                    <label class="form_file">
									                    <input class="form_file__input" type="file" name="" placeholder="" value="">
									                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
									                    <span class="form_file__text"><span></span></span>
									                    <span class="form_file__btn">Загрузить</span>
								                    </label>
							                    </div>
						                    </div>
					                    </div>

					                    <div class="form_group form_group_inline">
						                    <div class="row">
							                    <div class="col col-xs-12 col-lg-6 col-xl-5">
								                    <label class="form_label">Грустное изображение человека</label>
							                    </div>
							                    <div class="col col-xs-12 col-lg-6 col-xl-7">
								                    <label class="form_file">
									                    <input class="form_file__input" type="file" name="" placeholder="" value="">
									                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
									                    <span class="form_file__text"><span></span></span>
									                    <span class="form_file__btn">Загрузить</span>
								                    </label>
							                    </div>
						                    </div>
					                    </div>

					                    <div class="form_group form_group_inline">
						                    <div class="row">
							                    <div class="col col-xs-12 col-lg-6 col-xl-5">
								                    <label class="form_label">Нейтральное изображение человека</label>
							                    </div>
							                    <div class="col col-xs-12 col-lg-6 col-xl-7">
								                    <label class="form_file">
									                    <input class="form_file__input" type="file" name="" placeholder="" value="">
									                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
									                    <span class="form_file__text"><span></span></span>
									                    <span class="form_file__btn">Загрузить</span>
								                    </label>
							                    </div>
						                    </div>
					                    </div>

					                    <div class="form_group form_group_inline">
						                    <div class="row">
							                    <div class="col col-xs-12 col-lg-6 col-xl-5">
								                    <label class="form_label">Отрицательное изображение человека</label>
							                    </div>
							                    <div class="col col-xs-12 col-lg-6 col-xl-7">
								                    <label class="form_file">
									                    <input class="form_file__input" type="file" name="" placeholder="" value="">
									                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
									                    <span class="form_file__text"><span></span></span>
									                    <span class="form_file__btn">Загрузить</span>
								                    </label>
							                    </div>
						                    </div>
					                    </div>

					                    <div class="form_group form_group_inline">
						                    <div class="row">
							                    <div class="col col-xs-12 col-lg-6 col-xl-5">
								                    <label class="form_label">Успешное изображение человека</label>
							                    </div>
							                    <div class="col col-xs-12 col-lg-6 col-xl-7">
								                    <label class="form_file">
									                    <input class="form_file__input" type="file" name="" placeholder="" value="">
									                    <span class="form_file__icon">
                                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                                    </span>
									                    <span class="form_file__text"><span></span></span>
									                    <span class="form_file__btn">Загрузить</span>
								                    </label>
							                    </div>
						                    </div>
					                    </div>

				                    </div>
			                    </div>
		                    </div>

		                    <div class="form_box">
			                    <div class="row">
				                    <div class="col col-xs-12 col-xl-10 col-xl-offset-1">
					                    <div class="row">
						                    <div class="col col-xs-12 col-lg-6 col-lg-offset-6 col-xl-7 col-xl-offset-5">
							                    <h3>добавление диалога</h3>
						                    </div>
					                    </div>
				                    </div>
			                    </div>

			                    <div class="new_question">
				                    <div class="text-right">
					                    <a href="#" class="new_question__remove">Удалить вопрос</a>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-2 col-xl-2">
							                    <label class="form_label new_question__label">
								                    <span>Вопрос</span>
								                    <ul class="new_question__position">
									                    <li><a href="#">Вверх</a></li>
									                    <li><a href="#">Вниз</a></li>
								                    </ul>
							                    </label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-10 col-xl-10">
							                    <textarea class="form_control" name="" placeholder="" rows="4"></textarea>
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <div class="input_group">
								                    <input class="form_control" type="text" name="" placeholder="" value="">
								                    <div class="input_group_append">
									                    <div class="input_group_label new_question__status"><span class="color-green">верный</span></div>
								                    </div>
							                    </div>
						                    </div>
					                    </div>
					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Сообщение</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <div class="input_group">
								                    <input class="form_control" type="text" name="" placeholder="" value="">
								                    <div class="input_group_append">
									                    <div class="input_group_label new_question__status">нейтральный</div>
								                    </div>
							                    </div>
						                    </div>
					                    </div>
					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Сообщение</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <div class="input_group">
								                    <input class="form_control" type="text" name="" placeholder="" value="">
								                    <div class="input_group_append">
									                    <div class="input_group_label new_question__status"><span class="color-red">неверный</span></div>
								                    </div>
							                    </div>
						                    </div>
					                    </div>
					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Сообщение</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <div class="input_group">
								                    <input class="form_control" type="text" name="" placeholder="" value="">
								                    <div class="input_group_append">
									                    <div class="input_group_label new_question__status"><span class="color-red">неверный</span></div>
								                    </div>
							                    </div>
						                    </div>
					                    </div>
					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Сообщение</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

			                    </div>

			                    <div class="new_question">
				                    <div class="text-right">
					                    <a href="#" class="new_question__remove">Удалить вопрос</a>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-2 col-xl-2">
							                    <label class="form_label new_question__label">
								                    <span>Вопрос</span>
								                    <ul class="new_question__position">
									                    <li><a href="#">Вверх</a></li>
									                    <li><a href="#">Вниз</a></li>
								                    </ul>
							                    </label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-10 col-xl-10">
							                    <textarea class="form_control" name="" placeholder="" rows="4"></textarea>
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <div class="input_group">
								                    <input class="form_control" type="text" name="" placeholder="" value="">
								                    <div class="input_group_append">
									                    <div class="input_group_label new_question__status"><span class="color-green">верный</span></div>
								                    </div>
							                    </div>
						                    </div>
					                    </div>
					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Сообщение</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <div class="input_group">
								                    <input class="form_control" type="text" name="" placeholder="" value="">
								                    <div class="input_group_append">
									                    <div class="input_group_label new_question__status">нейтральный</div>
								                    </div>
							                    </div>
						                    </div>
					                    </div>
					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Сообщение</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <div class="input_group">
								                    <input class="form_control" type="text" name="" placeholder="" value="">
								                    <div class="input_group_append">
									                    <div class="input_group_label new_question__status"><span class="color-red">неверный</span></div>
								                    </div>
							                    </div>
						                    </div>
					                    </div>
					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Сообщение</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

				                    <div class="form_group form_group_inline">
					                    <div class="row mb-5">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Ответ</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <div class="input_group">
								                    <input class="form_control" type="text" name="" placeholder="" value="">
								                    <div class="input_group_append">
									                    <div class="input_group_label new_question__status"><span class="color-red">неверный</span></div>
								                    </div>
							                    </div>
						                    </div>
					                    </div>
					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-4">
							                    <label class="form_label form_label_md">Сообщение</label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-8">
							                    <input class="form_control" type="text" name="" placeholder="" value="">
						                    </div>
					                    </div>
				                    </div>

			                    </div>

			                    <div class="new_question">
				                    <div class="form_group text-right">
					                    <a href="#" class="btn btn_border_blue">ДОБАВИТЬ ВОПРОС</a>
				                    </div>
				                    <div class="form_group form_group_inline">
					                    <div class="row">
						                    <div class="col col-xs-12 col-md-3 col-lg-2 col-xl-2">
							                    <label class="form_label new_question__label">
								                    Последнее сообщение
							                    </label>
						                    </div>
						                    <div class="col col-xs-12 col-md-9 col-lg-10 col-xl-10">
							                    <textarea class="form_control" name="" placeholder="" rows="4"></textarea>
						                    </div>
					                    </div>
				                    </div>
				                    <div class="text-right">
					                    <button type="submit" class="btn btn_blue btn_send">СОХРАНИТЬ И РАЗМЕСТИТЬ НА ПОРТАЛЕ</button>
				                    </div>
			                    </div>

		                    </div>

	                    </div>

                    
                    </form>

                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
